Yes, you are correct. SIKE (Supersingular Isogeny Key Encapsulation) was indeed broken in 2022 by researchers Wouter Castryck and Thomas Decru. They discovered a vulnerability that allowed them to break the cryptographic security of SIKE using a classical computer in a relatively short amount of time.

Given this development, it would be surprising if Microsoft Research's post-quantum SSH implementation still relied on SIKE for key exchange after the vulnerability was disclosed. It's possible that they have since updated their protocols to use other post-quantum algorithms that remain secure against known attacks.

If you're looking into the current state of Microsoft's post-quantum SSH or any other cryptographic implementations, it's always best to refer to the latest official documentation or research papers from credible sources to ensure you have the most up-to-date information.

# Fri 21 Jun 10:42:35 CEST 2024 - Microsoft Research's post-quantum-ssh uses SIKE for key exchange. Didn't SIKE get proven insecure in 2022 by Castryck and Decru?